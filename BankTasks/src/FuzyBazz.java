public class FuzyBazz {
    public static void main(String[] args) {
        int number = 37;
        for (int i = 1; i < number; i++) {
            if (i % 15 == 0) {
                System.out.println("FuzzBuzz");
            } else if (i % 3 == 0) {
                System.out.println("Fuzz");
            } else if (i % 5 == 0) {
                System.out.println("Buzz");
            } else if (i % 3 != 0 && i % 5 != 0 && i % 15 != 0) {
                System.out.println(i);
            }
        }
    }
}

//1) gitlab account oching
//        2) Quyidagilarni bajarib bo'gach gitlabga yuklab silkani menga tashab qo'ying:
//        - Fibonacci (n ta fibonachini hisoblang)
//        - FizBuzz (n ta sonni fizbuzz ligini tekshiring)
//        - Palindrome (son yoki tekstni)
//        - Odd even sum
//        -Has duplicate element in array