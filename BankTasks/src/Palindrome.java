import java.util.Scanner;

public class Palindrome {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("So'zni kiriting: ");
        String word = scanner.next();

        boolean isPalindrome = true;
        for (int i = 0, j = word.length() - 1; i < word.length() / 2; i++, j--) {
            if (word.charAt(i) != word.charAt(j)) {
                isPalindrome = false;
            }
        }
        if (isPalindrome) {
            System.out.println(word + " palindrom");
        } else {
            System.out.println(word + " palindrom emas!");
        }
    }


}
//1) gitlab account oching
//        2) Quyidagilarni bajarib bo'gach gitlabga yuklab silkani menga tashab qo'ying:
//        - Fibonacci (n ta fibonachini hisoblang)
//        - FizBuzz (n ta sonni fizbuzz ligini tekshiring)
//        - Palindrome (son yoki tekstni)
//        - Odd even sum
//        -Has duplicate element in array